import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} />
          </td>
          <td>${item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleClickBuy(item);
                // this.props.handleUp(item);
              }}
              className="d-inline btn btn-danger"
            >
              +
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleRemove(item);
              }}
              className="btn btn-primary"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className="container mt-5">
        <thead>
          <tr>
            <td className="col-1 font-weight-bold ">ID</td>
            <td className="col-3 font-weight-bold">NAME</td>
            <td className="col-3 font-weight-bold">IMG</td>
            <td className="col-2 font-weight-bold">PRICE</td>
            <td className="font-weight-bold">QUANTITY</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
