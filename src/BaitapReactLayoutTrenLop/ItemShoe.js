import React, { Component } from "react";
import "./shoeShop.css";

export default class ItemShoe extends Component {
  render() {
    let { name, image, description, price } = this.props.data;

    return (
      <div className="col-4 p-3 ">
        <div className="card h-100  text-left">
          <img className="card-img-top" src={image} />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">${price}</p>
          </div>
          <div className="d-flex">
            <button
              onClick={() => {
                this.props.handleDetail(this.props.data);
              }}
              className=" btn btn-info p-3 font-weight-bold "
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handleBuy(this.props.data);
              }}
              className="btn btn-danger font-weight-bold"
            >
              Mua
            </button>
          </div>
        </div>
      </div>
    );
  }
}
