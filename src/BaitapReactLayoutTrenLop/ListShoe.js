import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return (
        <ItemShoe
          handleBuy={this.props.handleClickBuy}
          handleDetail={this.props.handleDetail}
          data={item}
          key={index}
        />
      );
    });
  };

  render() {
    return (
      <div className="row detail_shoe container mx-auto pt-5">
        {this.renderListShoe()}
      </div>
    );
  }
}
