import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { id, alias, price, description, image, name } = this.props.DetailShoe;

    return (
      <div className="detail_shoe bg-dark    ">
        <div
          className=" justify-content-center
d-flex col-12"
        >
          <div className="detail_photo col-4">
            <img src={image} />
          </div>
          <div className="text-left detail_body text-light col-8">
            <p>ID:{id}</p>
            <p>Tên:{name}</p>
            <p>Giá:${price}</p>
            <p>Mô Tả:{description}</p>
          </div>
        </div>
      </div>
    );
  }
}
