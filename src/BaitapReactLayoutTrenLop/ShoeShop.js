import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./DataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";
import "./shoeShop.css";

export default class ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: dataShoe[0],
    cart: [],
  };

  handleClickDetail = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };
  handleRemove = (shoe) => {
    let addcart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    console.log("á", shoe.number);

    addcart[index].number--;
    if (shoe.number <= 0) {
      addcart.splice(index, 1);
    }

    this.setState({
      cart: addcart,
    });
  };
  handleClickBuy = (shoe) => {
    let addCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let itemCart = { ...shoe, number: 1 };
      addCart.push(itemCart);
    } else {
      addCart[index].number++;
    }
    this.setState({
      cart: addCart,
    });
    console.log("cart: ", addCart);
  };

  render() {
    return (
      <div>
        <Cart
          handleRemove={this.handleRemove}
          handleClickBuy={this.handleClickBuy}
          cart={this.state.cart}
        />
        <ListShoe
          handleClickBuy={this.handleClickBuy}
          handleDetail={this.handleClickDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe DetailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
